/**
 * 
 */
package tk.tumelo.shared.core.security;

import org.eclipse.scout.rt.shared.security.BasicHierarchyPermission;

/**
 * @author sixkn_000
 */
public class DeleteCodePermission extends BasicHierarchyPermission {

  private static final long serialVersionUID = 1L;

  /**
   * 
   */
  public DeleteCodePermission() {
    super("DeleteCode");
  }
}