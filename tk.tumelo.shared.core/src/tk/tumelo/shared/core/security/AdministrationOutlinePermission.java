/**
 * 
 */
package tk.tumelo.shared.core.security;

import org.eclipse.scout.rt.shared.security.BasicHierarchyPermission;

/**
 * @author sixkn_000
 */
public class AdministrationOutlinePermission extends BasicHierarchyPermission {

  private static final long serialVersionUID = 1L;

  /**
   * 
   */
  public AdministrationOutlinePermission() {
    super("AdministrationOutline");
  }
}