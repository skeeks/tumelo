/**
 * 
 */
package tk.tumelo.shared.core.codes;

import java.security.BasicPermission;

/**
 * @author sixkn_000
 */
public class ReadCodePermission extends BasicPermission {

  private static final long serialVersionUID = 1L;

  /**
   * 
   */
  public ReadCodePermission() {
    super("ReadCode");
  }
}