/**
 * 
 */
package tk.tumelo.shared.core.codes;

import java.security.BasicPermission;

/**
 * @author sixkn_000
 */
public class CreateCodePermission extends BasicPermission {

  private static final long serialVersionUID = 1L;

  /**
   * 
   */
  public CreateCodePermission() {
    super("CreateCode");
  }
}