/**
 *
 */
package tk.tumelo.shared.core.codes;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;

/**
 * @author sixkn_000
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface ICodeProcessService extends IService {

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeFormData create(CodeFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeFormData load(CodeFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeFormData prepareCreate(CodeFormData formData) throws ProcessingException;

  /**
   * @param formData
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeFormData store(CodeFormData formData) throws ProcessingException;

  /**
   * @param ids
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  void delete(List<String> ids, Class<? extends AbstractDatabaseCodeType> codeTypeClass) throws ProcessingException;
}
