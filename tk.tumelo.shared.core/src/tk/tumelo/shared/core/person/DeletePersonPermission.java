/**
 *
 */
package tk.tumelo.shared.core.person;

import java.security.BasicPermission;

/**
 * @author sixkn_000
 */
public class DeletePersonPermission extends BasicPermission {

  private static final long serialVersionUID = 1L;

  /**
   *
   */
  public DeletePersonPermission() {
    super("UpdatePerson");
  }
}
