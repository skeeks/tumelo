/**
 * 
 */
package tk.tumelo.shared.core.person;

import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;
import org.eclipse.scout.commons.exception.ProcessingException;

/**
 * @author sixkn_000
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface IPersonPageService extends IService {

  /**
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  PersonTablePageData getTablePageData() throws ProcessingException;
}