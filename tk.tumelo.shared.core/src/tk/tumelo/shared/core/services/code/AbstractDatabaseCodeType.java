/**
 *
 */
package tk.tumelo.shared.core.services.code;

import java.util.List;

import org.eclipse.scout.commons.annotations.ConfigProperty;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeTypeWithGeneric;
import org.eclipse.scout.rt.shared.services.common.code.ICode;
import org.eclipse.scout.rt.shared.services.common.code.ICodeRow;
import org.eclipse.scout.service.SERVICES;

import tk.tumelo.shared.core.services.codetypes.IDatabaseCodeTypeLoaderService;

/**
 * @author sixkn_000
 */
public abstract class AbstractDatabaseCodeType extends AbstractCodeTypeWithGeneric<String, String, ICode<String>> {

  private static final long serialVersionUID = 1L;

  @ConfigProperty(ConfigProperty.BOOLEAN)
  public boolean getConfiguredIsAddingNewCodesEnabled() {
    return true;
  }

  @Override
  protected List<? extends ICodeRow<String>> execLoadCodes(Class<? extends ICodeRow<String>> codeRowType) throws ProcessingException {
    return SERVICES.getService(IDatabaseCodeTypeLoaderService.class).loadCodes(this.getId());
  }

}
