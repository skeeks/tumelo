/**
 *
 */
package tk.tumelo.shared.core.services.code;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;

/**
 * @author sixkn_000
 */
public class LanguageCodeType extends AbstractDatabaseCodeType {

  private static final long serialVersionUID = 1L;

  public static final String ID = "5647909834f8252cac3210a8";

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public LanguageCodeType() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredText() {
    return TEXTS.get("Language");
  }

  @Override
  public boolean getConfiguredIsAddingNewCodesEnabled() {
    return false;
  }

  @Override
  public String getId() {
    return ID;
  }

  @Order(1000.0)
  public static class GermanCode extends AbstractCode<String> {

    private static final long serialVersionUID = 1L;

    public static final String ID = "564790b634f8252df487c991";

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("German");
    }

    @Override
    public String getId() {
      return ID;
    }

    @Override
    protected String getConfiguredExtKey() {
      return "de";
    }
  }

  @Order(2000.0)
  public static class EnglishCode extends AbstractCode<String> {

    private static final long serialVersionUID = 1L;

    public static final String ID = "564790cb34f8251accff1c44";

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("English");
    }

    @Override
    public String getId() {
      return ID;
    }

    @Override
    protected String getConfiguredExtKey() {
      return "en";
    }
  }

  @Order(3000.0)
  public static class FrenchCode extends AbstractCode<String> {

    private static final long serialVersionUID = 1L;

    public static final String ID = "564790e034f825054465cfac";

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("French");
    }

    @Override
    public String getId() {
      return ID;
    }

    @Override
    protected boolean getConfiguredActive() {
      return false; // tumelo doesnt supports french
    }

    @Override
    protected String getConfiguredExtKey() {
      return "fr";
    }
  }

  @Order(4000.0)
  public static class ItalianCode extends AbstractCode<String> {

    private static final long serialVersionUID = 1L;

    public static final String ID = "564790f134f8252ccc9bd31d";

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Italian");
    }

    @Override
    public String getId() {
      return ID;
    }

    @Override
    protected boolean getConfiguredActive() {
      return false; // tumelo doesnt supports italian
    }

    @Override
    protected String getConfiguredExtKey() {
      return "it";
    }
  }
}
