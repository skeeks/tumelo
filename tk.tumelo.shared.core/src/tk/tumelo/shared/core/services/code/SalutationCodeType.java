/**
 *
 */
package tk.tumelo.shared.core.services.code;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;

/**
 * @author sixkn_000
 */
public class SalutationCodeType extends AbstractDatabaseCodeType {

  private static final long serialVersionUID = 1L;

  public static final String ID = "5647b39734f8252938d9afec";

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public SalutationCodeType() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredText() {
    return TEXTS.get("Salutation");
  }

  @Override
  public String getId() {
    return ID;
  }

  @Order(1000.0)
  public static class MrCode extends AbstractCode<String> {

    private static final long serialVersionUID = 1L;

    public static final String ID = "5647b3ab34f8251d202b4f1a";

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Mr");
    }

    @Override
    public String getId() {
      return ID;
    }
  }

  @Order(2000.0)
  public static class MissCode extends AbstractCode<String> {

    private static final long serialVersionUID = 1L;

    public static final String ID = "5647b3c034f82524e014251c";

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("Miss");
    }

    @Override
    public String getId() {
      return ID;
    }
  }
}
