/**
 *
 */
package tk.tumelo.shared.core.services.code;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;

/**
 * @author sixkn_000
 */
public class PersonAddressTypeCodeType extends AbstractDatabaseCodeType {

  private static final long serialVersionUID = 1L;
  /**
   *
   */
  public static final String ID = "56478a2d34f8251d44bd89f4";

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public PersonAddressTypeCodeType() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredText() {
    return TEXTS.get("PersonAddressType");
  }

  @Override
  public String getId() {
    return ID;
  }

  @Order(1000.0)
  public static class PrivateAddressCode extends AbstractCode<String> {

    private static final long serialVersionUID = 1L;

    public static final String ID = "5647aa9334f8250648e65e6f";

    @Override
    protected String getConfiguredText() {
      return TEXTS.get("PrivateAddress");
    }

    @Override
    public String getId() {
      return ID;
    }

    @Override
    protected String getConfiguredExtKey() {
      return "PRADDRESS";
    }
  }
}
