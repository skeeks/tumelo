/**
 * 
 */
package tk.tumelo.shared.core.services.codetypes;

import java.security.BasicPermission;

/**
 * @author sixkn_000
 */
public class ReadCodePagePermission extends BasicPermission {

  private static final long serialVersionUID = 1L;

  /**
   * 
   */
  public ReadCodePagePermission() {
    super("ReadCodePage");
  }
}