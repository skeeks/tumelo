/**
 *
 */
package tk.tumelo.shared.core.services.codetypes;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.service.IService;

/**
 * @author sixkn_000
 */
public interface IDatabaseCodeTypeInserterService extends IService {
  void insertCodeTypesIntoDb() throws ProcessingException;
}
