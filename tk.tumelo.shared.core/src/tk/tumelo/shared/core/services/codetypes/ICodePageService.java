/**
 *
 */
package tk.tumelo.shared.core.services.codetypes;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;
import tk.tumelo.shared.core.ui.desktop.outlines.CodeTypesTablePageData;
import tk.tumelo.shared.core.ui.desktop.outlines.CodesTablePageData;

/**
 * @author sixkn_000
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface ICodePageService extends IService {

  /**
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodeTypesTablePageData getCodeTypesTableData() throws ProcessingException;

  /**
   * @return
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  CodesTablePageData getCodesTableData(String codeTypeNr, Class<? extends AbstractDatabaseCodeType> codeTypeClazz) throws ProcessingException;
}
