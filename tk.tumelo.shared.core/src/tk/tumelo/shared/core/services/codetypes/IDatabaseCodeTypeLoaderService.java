/**
 *
 */
package tk.tumelo.shared.core.services.codetypes;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.services.common.code.ICodeRow;
import org.eclipse.scout.rt.shared.validate.IValidationStrategy;
import org.eclipse.scout.rt.shared.validate.InputValidation;
import org.eclipse.scout.service.IService;

/**
 * @author sixkn_000
 */
@InputValidation(IValidationStrategy.PROCESS.class)
public interface IDatabaseCodeTypeLoaderService extends IService {

  List<? extends ICodeRow<String>> loadCodes(String codeTypeId) throws ProcessingException;
}
