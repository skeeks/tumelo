package tk.tumelo.shared.core.services.common.text;

import java.util.Locale;

import org.eclipse.scout.rt.shared.services.common.code.CODES;
import org.eclipse.scout.rt.shared.services.common.text.AbstractDynamicNlsTextProviderService;

import tk.tumelo.shared.core.services.code.LanguageCodeType;

public class TumeloTextProviderService extends AbstractDynamicNlsTextProviderService {
  @Override
  protected String getDynamicNlsBaseName() {
    return "resources.texts.Texts";
  }

  /**
   * Get text for a keyword
   * 
   * @param key
   *          keyword for the text
   * @param languageId
   *          language id from {@link LanguageCodeType}
   * @param messageArguments
   *          The message arguments for the text
   * @return The text for the key <i>key</i> in the language <i>language id</i>
   */
  public String getText(String key, String languageId, String... messageArguments) {
    return this.getText(new Locale(CODES.getCodeType(LanguageCodeType.class).getCode(languageId).getExtKey()), key, messageArguments);
  }
}
