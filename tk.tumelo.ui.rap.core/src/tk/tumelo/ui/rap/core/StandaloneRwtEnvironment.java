package tk.tumelo.ui.rap.core;

import org.eclipse.scout.rt.ui.rap.AbstractStandaloneRwtEnvironment;

import tk.tumelo.client.core.ClientSession;

public class StandaloneRwtEnvironment extends AbstractStandaloneRwtEnvironment {

  private static final long serialVersionUID = 1L;

  public StandaloneRwtEnvironment() {
    super(Activator.getDefault().getBundle(), ClientSession.class);
  }
}
