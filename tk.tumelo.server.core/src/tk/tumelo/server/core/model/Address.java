/**
 *
 */
package tk.tumelo.server.core.model;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import tk.tumelo.server.core.services.mongodb.morphia.IModel;

/**
 * @author sixkn_000
 */
@Embedded
public class Address implements IModel {
  @Id
  private ObjectId id;
  @Property
  private String addressType;
  @Property
  private String additionalName;
  @Property
  private String street;
  @Property
  private String zipCode;
  @Property
  private String city;
  @Property
  private String phoneNumberPrivate;
  @Property
  private String phoneNumberMobile;
  @Property
  private String email;

  public Address() {

  }

  /**
   * @return the addressType
   */
  public String getAddressType() {
    return addressType;
  }

  /**
   * @param addressType
   *          the addressType to set
   */
  public void setAddressType(String addressType) {
    this.addressType = addressType;
  }

  /**
   * @return the id
   */
  public ObjectId getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(String id) {
    this.id = new ObjectId(id);
  }

  /**
   * @return the additionalName
   */
  public String getAdditionalName() {
    return additionalName;
  }

  /**
   * @param additionalName
   *          the additionalName to set
   */
  public void setAdditionalName(String additionalName) {
    this.additionalName = additionalName;
  }

  /**
   * @return the street
   */
  public String getStreet() {
    return street;
  }

  /**
   * @param street
   *          the street to set
   */
  public void setStreet(String street) {
    this.street = street;
  }

  /**
   * @return the zipCode
   */
  public String getZipCode() {
    return zipCode;
  }

  /**
   * @param zipCode
   *          the zipCode to set
   */
  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  /**
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * @param city
   *          the city to set
   */
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * @return the phoneNumberPrivate
   */
  public String getPhoneNumberPrivate() {
    return phoneNumberPrivate;
  }

  /**
   * @param phoneNumberPrivate
   *          the phoneNumberPrivate to set
   */
  public void setPhoneNumberPrivate(String phoneNumberPrivate) {
    this.phoneNumberPrivate = phoneNumberPrivate;
  }

  /**
   * @return the phoneNumberMobile
   */
  public String getPhoneNumberMobile() {
    return phoneNumberMobile;
  }

  /**
   * @param phoneNumberMobile
   *          the phoneNumberMobile to set
   */
  public void setPhoneNumberMobile(String phoneNumberMobile) {
    this.phoneNumberMobile = phoneNumberMobile;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email
   *          the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

}
