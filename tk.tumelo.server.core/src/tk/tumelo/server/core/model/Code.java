/**
 *
 */
package tk.tumelo.server.core.model;

import java.math.BigDecimal;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import tk.tumelo.server.core.ServerSession;
import tk.tumelo.server.core.services.mongodb.morphia.IModel;
import tk.tumelo.shared.core.services.code.LanguageCodeType;

/**
 * @author sixkn_000
 */
@Entity("codes")
public class Code implements IModel {

  @Id
  private ObjectId id;

  @Property
  private String codeType;
  @Property
  private boolean active;
  @Property
  private String backgroundColor;
  @Property
  private String foregroundColor;
  @Property
  private boolean enabled;
  @Property
  private String iconId;
  @Property
  private String parentKey;
  @Property
  private long partitionUid;
  @Property
  private BigDecimal value;
  @Property
  private double order;
  @Property
  private String extKey;

  @Property
  private String englishText;
  @Property
  private String germanText;
  @Property
  private String italianText;
  @Property
  private String frenchText;

  /**
   * @return the extKey
   */
  public String getExtKey() {
    return extKey;
  }

  /**
   * @param extKey
   *          the extKey to set
   */
  public void setExtKey(String extKey) {
    this.extKey = extKey;
  }

  /**
   * @return the englishText
   */
  public String getEnglishText() {
    return englishText;
  }

  /**
   * @param englishText
   *          the englishText to set
   */
  public void setEnglishText(String englishText) {
    this.englishText = englishText;
  }

  /**
   * @return the germanText
   */
  public String getGermanText() {
    return germanText;
  }

  /**
   * @param germanText
   *          the germanText to set
   */
  public void setGermanText(String germanText) {
    this.germanText = germanText;
  }

  /**
   * @return the italianText
   */
  public String getItalianText() {
    return italianText;
  }

  /**
   * @param italianText
   *          the italianText to set
   */
  public void setItalianText(String italianText) {
    this.italianText = italianText;
  }

  /**
   * @return the frenchText
   */
  public String getFrenchText() {
    return frenchText;
  }

  /**
   * @param frenchText
   *          the frenchText to set
   */
  public void setFrenchText(String frenchText) {
    this.frenchText = frenchText;
  }

  /**
   * @return the id
   */
  public String getId() {
    return id != null ? id.toHexString() : "null";
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(String id) {
    this.id = new ObjectId(id);
  }

  /**
   * @return the codeType
   */
  public String getCodeType() {
    return codeType;
  }

  /**
   * @param codeType
   *          the codeType to set
   */
  public void setCodeType(String codeType) {
    this.codeType = codeType;
  }

  /**
   * @return the active
   */
  public boolean isActive() {
    return active;
  }

  /**
   * @param active
   *          the active to set
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  /**
   * @return the backgroundColor
   */
  public String getBackgroundColor() {
    return backgroundColor;
  }

  /**
   * @param backgroundColor
   *          the backgroundColor to set
   */
  public void setBackgroundColor(String backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  /**
   * @return the foregroundColor
   */
  public String getForegroundColor() {
    return foregroundColor;
  }

  /**
   * @param foregroundColor
   *          the foregroundColor to set
   */
  public void setForegroundColor(String foregroundColor) {
    this.foregroundColor = foregroundColor;
  }

  /**
   * @return the enabled
   */
  public boolean isEnabled() {
    return enabled;
  }

  /**
   * @param enabled
   *          the enabled to set
   */
  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  /**
   * @return the iconId
   */
  public String getIconId() {
    return iconId;
  }

  /**
   * @param iconId
   *          the iconId to set
   */
  public void setIconId(String iconId) {
    this.iconId = iconId;
  }

  /**
   * @return the parentKey
   */
  public String getParentKey() {
    return parentKey;
  }

  /**
   * @param parentKey
   *          the parentKey to set
   */
  public void setParentKey(String parentKey) {
    this.parentKey = parentKey;
  }

  /**
   * @return the partitionUid
   */
  public long getPartitionUid() {
    return partitionUid;
  }

  /**
   * @param partitionUid
   *          the partitionUid to set
   */
  public void setPartitionUid(long partitionUid) {
    this.partitionUid = partitionUid;
  }

  /**
   * @return the value
   */
  public BigDecimal getValue() {
    return value;
  }

  /**
   * @param value
   *          the value to set
   */
  public void setValue(BigDecimal value) {
    this.value = value;
  }

  /**
   * @return the order
   */
  public double getOrder() {
    return order;
  }

  /**
   * @param order
   *          the order to set
   */
  public void setOrder(double order) {
    this.order = order;
  }

  /**
   * @return
   */
  public String getTextInCurrentLanguage() {
    switch (ServerSession.get().getLanguageId()) {
      case LanguageCodeType.EnglishCode.ID:
        return getEnglishText();
      case LanguageCodeType.GermanCode.ID:
        return getGermanText();
      case LanguageCodeType.ItalianCode.ID:
        return getItalianText();
      case LanguageCodeType.FrenchCode.ID:
        return getFrenchText();
      default:
        return getEnglishText();
    }
  }

}
