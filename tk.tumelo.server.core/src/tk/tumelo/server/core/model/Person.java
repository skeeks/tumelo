package tk.tumelo.server.core.model;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import tk.tumelo.server.core.services.mongodb.morphia.IModel;

/**
 * @author sixkn_000
 */
@Entity("persons")
public class Person implements IModel {

  @Id
  private ObjectId id;

  @Property
  private String firstName;

  @Property
  private String lastName;

  @Property
  private String salutation;

  @Property
  private String title;

  @Property
  private String notes;

  private List<Address> addresses = new ArrayList<Address>();

  public Person() {

  }

  public Person(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * @return the id
   */
  public ObjectId getId() {
    return id;
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @return the notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes
   *          the notes to set
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public void setId(String hexId) {
    this.id = new ObjectId(hexId);
  }

  /**
   * @return the salutation
   */
  public String getSalutation() {
    return salutation;
  }

  /**
   * @param salutation
   *          the salutation to set
   */
  public void setSalutation(String salutation) {
    this.salutation = salutation;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title
   *          the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the addresses
   */
  public List<Address> getAddresses() {
    return addresses;
  }

  /**
   * @param addresses
   *          the addresses to set
   */
  public void setAddresses(List<Address> addresses) {
    this.addresses = addresses;
  }

}
