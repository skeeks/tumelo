package tk.tumelo.server.core;

import java.util.List;
import java.util.Locale;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.logger.IScoutLogger;
import org.eclipse.scout.commons.logger.ScoutLogManager;
import org.eclipse.scout.rt.server.AbstractServerSession;
import org.eclipse.scout.rt.server.ServerJob;
import org.eclipse.scout.rt.shared.services.common.code.CODES;
import org.eclipse.scout.rt.shared.services.common.code.ICode;

import tk.tumelo.shared.core.services.code.LanguageCodeType;

public class ServerSession extends AbstractServerSession {
  private static final long serialVersionUID = 1L;
  private static final IScoutLogger LOG = ScoutLogManager.getLogger(ServerSession.class);

  public ServerSession() {
    super(true);
  }

  /**
   * @return session in current ThreadContext
   */
  public static ServerSession get() {
    return ServerJob.getCurrentSession(ServerSession.class);
  }

  @Override
  protected void execLoadSession() throws ProcessingException {
    LOG.info("created a new session for " + getUserId());
    setLanguageId(determineUserLanguageid());
  }

  protected String determineUserLanguageid() {
    Locale locale = getLocale();

    List<? extends ICode<String>> codes = CODES.getCodeType(LanguageCodeType.class).getCodes();

    for (ICode<String> code : codes) {
      if (locale.getLanguage().equals(new Locale(code.getExtKey()).getLanguage())) {
        return code.getId();
      }
    }
    return LanguageCodeType.EnglishCode.ID; // fallback if no language was matched.
  }

  public String getLanguageId() {
    return getSharedContextVariable("languageId", String.class);
  }

  public void setLanguageId(String language) {
    setSharedContextVariable("languageId", String.class, language);
  }
}
