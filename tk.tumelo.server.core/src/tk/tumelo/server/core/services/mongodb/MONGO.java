/**
 *
 */
package tk.tumelo.server.core.services.mongodb;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.service.SERVICES;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;

import tk.tumelo.server.core.services.mongodb.morphia.IModel;
import tk.tumelo.server.core.services.mongodb.morphia.IMorphiaService;

/**
 * @author sixkn_000
 */
public final class MONGO {
  private static Class<? extends IMorphiaService> service = IMorphiaService.class;

  private MONGO() {

  }

  public static Datastore getDatastore() throws ProcessingException {
    return SERVICES.getService(service).getDatastore();
  }

  public static <E extends IModel> Key<E> save(E entity) throws ProcessingException {
    return SERVICES.getService(service).save(entity);
  }

  /**
   * @param personNr
   * @return
   */
  public static <E extends IModel> E load(Class<E> entityClazz, String entityId) throws ProcessingException {
    return SERVICES.getService(service).load(entityClazz, entityId);
  }

  /**
   * @param person
   */
  public static <E extends IModel> E store(E person) throws ProcessingException {
    return SERVICES.getService(service).store(person);
  }

  /**
   * @param entityId
   */
  public static <E extends IModel> void delete(Class<E> entityClass, List<String> entityIds) throws ProcessingException {
    SERVICES.getService(service).delete(entityClass, entityIds);
  }

  /**
   * @param class1
   * @return
   */
  public static <E extends IModel> Query<E> findAll(Class<E> entityClazz) throws ProcessingException {
    return SERVICES.getService(service).findAll(entityClazz);
  }

  /**
   * @param codesToInsert
   */

  public static <E extends IModel> List<Key<E>> save(List<E> entities) throws ProcessingException {
    return SERVICES.getService(service).save(entities);
  }
}
