/**
 *
 */
package tk.tumelo.server.core.services.mongodb.morphia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.types.ObjectId;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.service.AbstractService;
import org.eclipse.scout.service.SERVICES;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import tk.tumelo.server.core.model.Person;
import tk.tumelo.server.core.services.mongodb.IMongoDbService;

/**
 * @author sixkn_000
 */
public class MorphiaService extends AbstractService implements IMorphiaService {
  private final Morphia morphia;

  private Datastore m_datastore;

  public MorphiaService() {
    morphia = new Morphia();
    initConfig();

  }

  protected void initConfig() {
    IMongoDbService svc = SERVICES.getService(IMongoDbService.class);
    morphia.map(Person.class);
    this.setDatastore(morphia.createDatastore(svc.getClient(), svc.getDatabaseName())); //best to use (Mongo, String) method, where Mongo is a singleton.

    //at application start map classes before calling with morphia map* methods
    getDatastore().ensureIndexes(); //creates indexes from @Index annotations in your entities
    getDatastore().ensureCaps(); //creates capped collections from @Entity
  }

  /**
   * @return the datastore
   */
  @Override
  public Datastore getDatastore() {
    return m_datastore;
  }

  /**
   * @param datastore
   *          the datastore to set
   */
  protected void setDatastore(Datastore datastore) {
    m_datastore = datastore;
  }

  /**
   * @return the morphia
   */
  public Morphia getMorphia() {
    return morphia;
  }

  @Override
  public <E extends IModel> Key<E> save(E entity) {
    return getDatastore().save(entity);
  }

  /**
   * Get the entity with the given id
   */
  @Override
  public <E extends IModel> E load(Class<E> entityClazz, String entityId) throws ProcessingException {
    return getDatastore().get(entityClazz, new ObjectId(entityId));
  }

  @Override
  public <E extends IModel> List<E> load(Class<E> entityClazz, List<String> entityId) throws ProcessingException {
    Query<E> query = getDatastore().createQuery(entityClazz).field("_id").in(entityId);
    return query.asList();
  }

  @Override
  public <E extends IModel> E store(E entity) throws ProcessingException {
    getDatastore().save(entity);
    return entity;
  }

  @Override
  public <E extends IModel> void delete(Class<E> entityClazz, String entityId) throws ProcessingException {
    this.delete(entityClazz, Arrays.asList(entityId));
  }

  @Override
  public <E extends IModel> void delete(Class<E> entityClazz, List<String> entityIds) throws ProcessingException {
    Query<E> query = getDatastore().createQuery(entityClazz).field("_id").in(entityIds);
    getDatastore().delete(query);
  }

  @Override
  public <E extends IModel> Query<E> findAll(Class<E> entityClazz) throws ProcessingException {
    return getDatastore().find(entityClazz);
  }

  @Override
  public <E extends IModel> List<Key<E>> save(List<E> entities) throws ProcessingException {
    Iterable<Key<E>> result = getDatastore().save(entities);
    List<Key<E>> savedKeys = new ArrayList<Key<E>>();
    for (Key<E> key : result) {
      savedKeys.add(key);
    }
    return savedKeys;
  }

}
