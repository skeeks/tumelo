/**
 *
 */
package tk.tumelo.server.core.services.mongodb;

import java.util.Arrays;

import org.bson.Document;
import org.eclipse.scout.commons.annotations.ConfigProperty;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.annotations.Priority;
import org.eclipse.scout.service.AbstractService;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * @author ske
 * @since 0.0.1
 */
@Priority(-1)
public abstract class AbstractMongoDbService extends AbstractService implements IMongoDbService {

  private MongoClient m_client;
  private String m_username;
  private String m_password;
  private String m_host;
  private int m_port;
  private String m_databaseName;

  public AbstractMongoDbService() {
    initConfig();
  }

  protected void initConfig() {
    setUsername(getConfiguredUsername());
    setPassword(getConfiguredPassword());
    setHost(getConfiguredHost());
    setPort(getConfiguredPort());
    setDatabaseName(getConfiguredDatabaseName());
  }

  @ConfigProperty(ConfigProperty.STRING)
  @Order(10)
  protected String getConfiguredUsername() {
    return null;
  }

  @ConfigProperty(ConfigProperty.STRING)
  @Order(20)
  protected String getConfiguredPassword() {
    return null;
  }

  @ConfigProperty(ConfigProperty.STRING)
  @Order(30)
  protected String getConfiguredHost() {
    return null;
  }

  @ConfigProperty(ConfigProperty.STRING)
  @Order(40)
  protected String getConfiguredDatabaseName() {
    return null;
  }

  @ConfigProperty(ConfigProperty.INTEGER)
  @Order(50)
  protected int getConfiguredPort() {
    return 27017;
  }

  @Override
  public MongoDatabase getDatabase() {
    if (getClient() == null) {
      this.connect();
    }
    return getClient().getDatabase(getDatabaseName());
  }

  private void connect() {
    MongoCredential credential = MongoCredential.createCredential(getUsername(), getDatabaseName(), (getPassword() == null ? new char[0] : getPassword().toCharArray()));
    this.m_client = new MongoClient(new ServerAddress(getHost()), Arrays.asList(credential));
  }

  private void disconnect() {
    getClient().close();
  }

  @Override
  public void disposeServices() {
    this.disconnect();
  }

  @Override
  public MongoCollection<Document> getCollection(String name) {
    return getDatabase().getCollection(name);
  }

  /*
   * Getter and Setters
   */

  /**
   * @return the client
   */
  @Override
  public MongoClient getClient() {
    if (this.m_client == null) {
      this.connect();
    }
    return m_client;
  }

  /**
   * @return the username
   */
  @Override
  public String getUsername() {
    return m_username;
  }

  /**
   * @param username
   *          the username to set
   */
  public void setUsername(String username) {
    m_username = username;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return m_password;
  }

  /**
   * @param password
   *          the password to set
   */
  public void setPassword(String password) {
    m_password = password;
  }

  /**
   * @return the host
   */
  public String getHost() {
    return m_host;
  }

  /**
   * @param host
   *          the host to set
   */
  public void setHost(String host) {
    m_host = host;
  }

  /**
   * @return the port
   */
  public int getPort() {
    return m_port;
  }

  /**
   * @param port
   *          the port to set
   */
  public void setPort(int port) {
    m_port = port;
  }

  /**
   * @return the databaseName
   */
  @Override
  public String getDatabaseName() {
    return m_databaseName;
  }

  /**
   * @param databaseName
   *          the databaseName to set
   */
  public void setDatabaseName(String databaseName) {
    m_databaseName = databaseName;
  }

}
