/**
 *
 */
package tk.tumelo.server.core.services.mongodb.morphia;

/**
 * Represents a class that can be persisted by MorphiaService
 */
public interface IModel {
}
