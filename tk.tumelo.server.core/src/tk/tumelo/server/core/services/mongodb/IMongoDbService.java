/**
 *
 */
package tk.tumelo.server.core.services.mongodb;

import org.bson.Document;
import org.eclipse.scout.service.IService;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * @author sixkn_000
 */
public interface IMongoDbService extends IService {
  MongoDatabase getDatabase();

  MongoCollection<Document> getCollection(String name);

  MongoClient getClient();

  String getDatabaseName();

  String getUsername();
}
