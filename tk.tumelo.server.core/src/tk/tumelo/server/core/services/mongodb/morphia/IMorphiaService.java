/**
 *
 */
package tk.tumelo.server.core.services.mongodb.morphia;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;

/**
 * @author sixkn_000
 */
public interface IMorphiaService {

  Datastore getDatastore() throws ProcessingException;

  <E extends IModel> Key<E> save(E entity) throws ProcessingException;

  <E extends IModel> List<Key<E>> save(List<E> entities) throws ProcessingException;

  <E extends IModel> E load(Class<E> entityClazz, String entityId) throws ProcessingException;

  <E extends IModel> List<E> load(Class<E> entityClazz, List<String> entityId) throws ProcessingException;

  <E extends IModel> E store(E person) throws ProcessingException;

  /**
   * Delete a entity with a specific id.
   *
   * @param entityClazz
   * @param entityId
   */
  <E extends IModel> void delete(Class<E> entityClazz, String entityId) throws ProcessingException;

  /**
   * Delete all entity with the specific ids.
   *
   * @param entityClazz
   * @param entityId
   * @throws ProcessingException
   */
  <E extends IModel> void delete(Class<E> entityClazz, List<String> entityId) throws ProcessingException;

  <E extends IModel> Query<E> findAll(Class<E> entityClazz) throws ProcessingException;
}
