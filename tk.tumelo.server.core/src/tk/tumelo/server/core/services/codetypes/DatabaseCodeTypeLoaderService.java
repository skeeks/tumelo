/**
 *
 */
package tk.tumelo.server.core.services.codetypes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.shared.services.common.code.CodeRow;
import org.eclipse.scout.rt.shared.services.common.code.ICodeRow;
import org.eclipse.scout.service.AbstractService;

import tk.tumelo.server.core.model.Code;
import tk.tumelo.server.core.services.mongodb.MONGO;
import tk.tumelo.shared.core.services.codetypes.IDatabaseCodeTypeLoaderService;

/**
 * @author sixkn_000
 */
public class DatabaseCodeTypeLoaderService extends AbstractService implements IDatabaseCodeTypeLoaderService {

  /**
   * @return all available codes of the code type with the UID codeTypeUid.
   */
  @Override
  public List<? extends ICodeRow<String>> loadCodes(String codeTypeUid) throws ProcessingException {
    List<Code> codes = MONGO.getDatastore().createQuery(Code.class).field("codeType").equal(codeTypeUid).asList();

    List<ICodeRow<String>> codeRows = new ArrayList<ICodeRow<String>>();
    for (Code code : codes) {
      codeRows.add(mapToCodeRow(code));
    }
    return codeRows;
  }

  protected ICodeRow<String> mapToCodeRow(Code code) {
    ICodeRow<String> codeRow = new CodeRow<String>(code.getId(), code.getTextInCurrentLanguage());
    codeRow.setActive(code.isActive());
    codeRow.setBackgroundColor(code.getBackgroundColor());
    codeRow.setForegroundColor(code.getForegroundColor());
    codeRow.setEnabled(code.isEnabled());
    codeRow.setIconId(code.getIconId());
    codeRow.setParentKey(code.getParentKey());
    codeRow.setPartitionId(code.getPartitionUid());
    codeRow.setValue(code.getValue());
    codeRow.setOrder(code.getOrder());
    return codeRow;
  }
}
