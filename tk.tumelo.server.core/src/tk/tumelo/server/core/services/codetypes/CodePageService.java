/**
 *
 */
package tk.tumelo.server.core.services.codetypes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.CODES;
import org.eclipse.scout.rt.shared.services.common.code.ICode;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.eclipse.scout.service.AbstractService;

import tk.tumelo.server.core.model.Code;
import tk.tumelo.server.core.services.mongodb.MONGO;
import tk.tumelo.shared.core.Activator;
import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;
import tk.tumelo.shared.core.services.codetypes.ICodePageService;
import tk.tumelo.shared.core.services.codetypes.ReadCodePagePermission;
import tk.tumelo.shared.core.ui.desktop.outlines.CodeTypesTablePageData;
import tk.tumelo.shared.core.ui.desktop.outlines.CodeTypesTablePageData.CodeTypesTableRowData;
import tk.tumelo.shared.core.ui.desktop.outlines.CodesTablePageData;
import tk.tumelo.shared.core.ui.desktop.outlines.CodesTablePageData.CodesTableRowData;

/**
 * @author sixkn_000
 */
public class CodePageService extends AbstractService implements ICodePageService {

  @Override
  public CodeTypesTablePageData getCodeTypesTableData() throws ProcessingException {
    if (!ACCESS.check(new ReadCodePagePermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    List<ICodeType<?, ?>> codeTypes = CODES.getAllCodeTypes(Activator.PLUGIN_ID);

    CodeTypesTablePageData pageData = new CodeTypesTablePageData();
    for (ICodeType<?, ?> codeType : codeTypes) {
      if (codeType instanceof AbstractDatabaseCodeType) {
        AbstractDatabaseCodeType dbCodeType = (AbstractDatabaseCodeType) codeType;
        CodeTypesTableRowData rowData = pageData.addRow();
        rowData.setCodeTypeNr(dbCodeType.getId());
        rowData.setName(dbCodeType.getText());
        rowData.setCodeTypeClass(dbCodeType.getClass());
      }
    }
    return pageData;
  }

  @Override
  public CodesTablePageData getCodesTableData(String codeTypeNr, Class<? extends AbstractDatabaseCodeType> codeTypeClazz) throws ProcessingException {
    if (!ACCESS.check(new ReadCodePagePermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }

    AbstractDatabaseCodeType codeType = CODES.getCodeType(codeTypeClazz);
    List<? extends ICode<String>> loadedCodes = codeType.getCodes(false);
    List<ObjectId> codeIds = new ArrayList<ObjectId>();
    for (ICode<String> loadedCode : loadedCodes) {
      codeIds.add(new ObjectId(loadedCode.getId()));
    }

    List<Code> codes = MONGO.getDatastore().createQuery(Code.class).field("id").in(codeIds).asList();

    CodesTablePageData pageData = new CodesTablePageData();
    pageData.setAddingNewCodesAllowed(codeType.getConfiguredIsAddingNewCodesEnabled());
    for (Code code : codes) {
      CodesTableRowData rowData = pageData.addRow();
      rowData.setCodeId(code.getId());
      rowData.setEnglishText(code.getEnglishText());
      rowData.setGermanText(code.getGermanText());
      rowData.setValue(code.getValue());
      rowData.setExtensionKey(code.getExtKey());
      rowData.setOrder(new BigDecimal(code.getOrder()));
      rowData.setProtected(false); // codes from the database are not protected
      // track which codes are loaded from the database and which not.
      codeIds.remove(new ObjectId(code.getId()));
    }

    // add all codes which are not loaded from DB to the page data  and set them to protected = true !IMPORTANT!
    for (ObjectId id : codeIds) {
      CodesTableRowData rowData = pageData.addRow();
      ICode<String> code = codeType.getCode(id.toHexString());
      rowData.setCodeId(code.getId());
      rowData.setEnglishText(code.getText());
      rowData.setGermanText(code.getText());
      rowData.setValue((BigDecimal) code.getValue());
      rowData.setExtensionKey(code.getExtKey());
      rowData.setOrder(new BigDecimal(code.getOrder()));
      rowData.setProtected(true);
    }

    return pageData;
  }

}
