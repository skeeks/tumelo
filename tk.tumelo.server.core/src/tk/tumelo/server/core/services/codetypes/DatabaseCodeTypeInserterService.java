///**
// *
// */
//package tk.tumelo.server.core.services.codetypes;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.bson.types.ObjectId;
//import org.eclipse.scout.commons.ConfigurationUtility;
//import org.eclipse.scout.commons.exception.ProcessingException;
//import org.eclipse.scout.commons.logger.IScoutLogger;
//import org.eclipse.scout.commons.logger.ScoutLogManager;
//import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
//import org.eclipse.scout.rt.shared.services.common.code.CODES;
//import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
//import org.eclipse.scout.service.AbstractService;
//
//import tk.tumelo.server.core.model.Code;
//import tk.tumelo.server.core.services.mongodb.MONGO;
//import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;
//import tk.tumelo.shared.core.services.codetypes.IDatabaseCodeTypeInserterService;
//
///**
// * @author sixkn_000
// */
//public class DatabaseCodeTypeInserterService extends AbstractService implements IDatabaseCodeTypeInserterService {
//  private static final IScoutLogger LOG = ScoutLogManager.getLogger(DatabaseCodeTypeInserterService.class);
//
//  /**
//   * @throws org.eclipse.scout.commons.exception.ProcessingException
//   */
//  @Override
//  public void insertCodeTypesIntoDb() throws ProcessingException {
//    LOG.info("Inserting holy database code types into database");
//    List<ICodeType<?, ?>> codeTypes = CODES.getAllCodeTypes(tk.tumelo.shared.core.Activator.PLUGIN_ID);
//
//    List<AbstractDatabaseCodeType> dbCodeTypes = findDatabaseCodeTypes(codeTypes);
//
//    List<Code> codesToInsert;
//    try {
//      codesToInsert = getCodesToInsert(dbCodeTypes);
//    }
//    catch (InstantiationException | IllegalAccessException e) {
//      e.printStackTrace();
//      throw new ProcessingException("CodeTypes could not be inserted to database.");
//    }
//
//    MONGO.save(codesToInsert);
//    LOG.info(codesToInsert.size() + " Holy Codes inserted!");
//  }
//
//  protected List<AbstractDatabaseCodeType> findDatabaseCodeTypes(List<ICodeType<?, ?>> availableCodeTypes) {
//    List<AbstractDatabaseCodeType> codeTypes = new ArrayList<AbstractDatabaseCodeType>();
//    for (ICodeType<?, ?> codeType : availableCodeTypes) {
//      if (codeType instanceof AbstractDatabaseCodeType) {
//        codeTypes.add((AbstractDatabaseCodeType) codeType);
//      }
//    }
//    return codeTypes;
//  }
//
//  @SuppressWarnings("unchecked")
//  protected List<Code> getCodesToInsert(List<AbstractDatabaseCodeType> dbCodeTypes) throws InstantiationException, IllegalAccessException {
//    List<Code> codes = new ArrayList<Code>();
//    for (AbstractDatabaseCodeType codeType : dbCodeTypes) {
//      Class<?>[] clazzes = ConfigurationUtility.getDeclaredPublicClasses(codeType.getClass());
//      for (Class<?> clazz : clazzes) {
//        if (AbstractCode.class.isAssignableFrom(clazz)) {
//          codes.add(mapToEntity((Class<AbstractCode<String>>) clazz, codeType));
//        }
//      }
//    }
//    return codes;
//  }
//
//  protected Code mapToEntity(Class<AbstractCode<String>> codeClazz, ICodeType<?, String> codeType) throws InstantiationException, IllegalAccessException {
//    AbstractCode<String> code = codeClazz.newInstance();
//    code.setCodeTypeInternal(codeType);
//    Code codeEntity = new Code();
//    codeEntity.setId(new ObjectId(code.getId()));
//    codeEntity.setActive(code.isActive());
//    codeEntity.setBackgroundColor(code.getBackgroundColor());
//    codeEntity.setCodeType(((AbstractDatabaseCodeType) code.getCodeType()).getId());
//    codeEntity.setForegroundColor(code.getForegroundColor());
//    codeEntity.setEnabled(code.isEnabled());
//    codeEntity.setIconId(code.getIconId());
//    codeEntity.setOrder(code.getOrder());
//    if (code.getParentCode() != null) {
//      codeEntity.setParentKey(code.getParentCode().getId());
//    }
//    codeEntity.setPartitionUid(code.getPartitionId());
//    codeEntity.setValue(code.getValue());
//    codeEntity.setExtKey(code.getExtKey());
//    codeEntity.setHoly(true); // set it to HOLY!
//    codeEntity.setEnglishText(code.getClass().getMethod("getConfiguredText").get);
//    return codeEntity;
//  }
//}
