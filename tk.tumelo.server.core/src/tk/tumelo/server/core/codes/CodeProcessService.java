/**
 *
 */
package tk.tumelo.server.core.codes;

import java.util.List;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.CODES;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.eclipse.scout.service.AbstractService;

import tk.tumelo.server.core.model.Code;
import tk.tumelo.server.core.services.mongodb.MONGO;
import tk.tumelo.shared.core.codes.CodeFormData;
import tk.tumelo.shared.core.codes.CreateCodePermission;
import tk.tumelo.shared.core.codes.ICodeProcessService;
import tk.tumelo.shared.core.codes.ReadCodePermission;
import tk.tumelo.shared.core.codes.UpdateCodePermission;
import tk.tumelo.shared.core.security.DeleteCodePermission;
import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;

/**
 * @author sixkn_000
 */
public class CodeProcessService extends AbstractService implements ICodeProcessService {

  @Override
  public CodeFormData create(CodeFormData formData) throws ProcessingException {
    if (!ACCESS.check(new CreateCodePermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    Code code = new Code();
    code.setCodeType(formData.getCodeTypeNr());
    code.setActive(formData.getActive().getValue());
    code.setEnabled(formData.getEnabled().getValue());
    code.setBackgroundColor(formData.getBackgroundColor().getValue());
    code.setForegroundColor(formData.getForegroundColor().getValue());
    code.setEnglishText(formData.getEnglishText().getValue());
    code.setFrenchText(formData.getFrenchText().getValue());
    code.setItalianText(formData.getItalianText().getValue());
    code.setGermanText(formData.getGermanText().getValue());
    code.setExtKey(formData.getExtensionKey().getValue());
    code.setValue(formData.getValue().getValue());
    MONGO.save(code);
    formData.setCodeNr(code.getId());
    CODES.reloadCodeType(formData.getCodeTypeClass());
    return formData;
  }

  @Override
  public CodeFormData load(CodeFormData formData) throws ProcessingException {
    if (!ACCESS.check(new ReadCodePermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }

    Code code = MONGO.load(Code.class, formData.getCodeNr());

    formData.setCodeTypeNr(code.getCodeType());
    formData.getActive().setValue(code.isActive());
    formData.getEnabled().setValue(code.isEnabled());
    formData.getBackgroundColor().setValue(code.getBackgroundColor());
    formData.getForegroundColor().setValue(code.getForegroundColor());
    formData.getEnglishText().setValue(code.getEnglishText());
    formData.getGermanText().setValue(code.getGermanText());
    formData.getFrenchText().setValue(code.getFrenchText());
    formData.getItalianText().setValue(code.getItalianText());
    formData.getExtensionKey().setValue(code.getExtKey());
    formData.getValue().setValue(code.getValue());

    return formData;
  }

  @Override
  public CodeFormData prepareCreate(CodeFormData formData) throws ProcessingException {
    if (!ACCESS.check(new CreateCodePermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    formData.getActive().setValue(true);
    formData.getEnabled().setValue(true);
    return formData;
  }

  @Override
  public CodeFormData store(CodeFormData formData) throws ProcessingException {
    if (!ACCESS.check(new UpdateCodePermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    Code code = new Code();
    code.setId(formData.getCodeNr());
    code.setCodeType(formData.getCodeTypeNr());
    code.setActive(formData.getActive().getValue());
    code.setEnabled(formData.getEnabled().getValue());
    code.setBackgroundColor(formData.getBackgroundColor().getValue());
    code.setForegroundColor(formData.getForegroundColor().getValue());
    code.setEnglishText(formData.getEnglishText().getValue());
    code.setFrenchText(formData.getFrenchText().getValue());
    code.setItalianText(formData.getItalianText().getValue());
    code.setGermanText(formData.getGermanText().getValue());
    code.setExtKey(formData.getExtensionKey().getValue());
    code.setValue(formData.getValue().getValue());

    MONGO.save(code);

    CODES.reloadCodeType(formData.getCodeTypeClass());
    return formData;
  }

  @Override
  public void delete(List<String> ids, Class<? extends AbstractDatabaseCodeType> codeTypeClass) throws ProcessingException {
    if (!ACCESS.check(new DeleteCodePermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    MONGO.delete(Code.class, ids);
    CODES.reloadCodeType(codeTypeClass);

  }
}
