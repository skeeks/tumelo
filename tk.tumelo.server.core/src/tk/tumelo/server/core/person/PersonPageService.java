/**
 *
 */
package tk.tumelo.server.core.person;

import java.util.function.Consumer;

import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.eclipse.scout.service.AbstractService;
import org.mongodb.morphia.query.Query;

import tk.tumelo.server.core.model.Person;
import tk.tumelo.server.core.services.mongodb.MONGO;
import tk.tumelo.shared.core.person.IPersonPageService;
import tk.tumelo.shared.core.person.PersonTablePageData;
import tk.tumelo.shared.core.person.PersonTablePageData.PersonTableRowData;
import tk.tumelo.shared.core.person.ReadPersonPermission;

/**
 * @author sixkn_000
 */
public class PersonPageService extends AbstractService implements IPersonPageService {

  @Override
  public PersonTablePageData getTablePageData() throws ProcessingException {
    if (!ACCESS.check(new ReadPersonPermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    Query<Person> query = MONGO.findAll(Person.class);
    final PersonTablePageData pageData = new PersonTablePageData();
    query.forEach(new Consumer<Person>() {

      @Override
      public void accept(Person person) {
        PersonTableRowData row = pageData.addRow();
        row.setFirstName(person.getFirstName());
        row.setLastName(person.getLastName());
        row.setPersonNr(person.getId().toHexString());
      }
    });
    return pageData;
  }
}
