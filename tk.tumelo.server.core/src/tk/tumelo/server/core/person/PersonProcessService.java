/**
 *
 */
package tk.tumelo.server.core.person;

import java.util.List;

import org.bson.types.ObjectId;
import org.eclipse.scout.commons.StringUtility;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.commons.exception.VetoException;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;
import org.eclipse.scout.service.AbstractService;

import tk.tumelo.server.core.model.Address;
import tk.tumelo.server.core.model.Person;
import tk.tumelo.server.core.services.mongodb.MONGO;
import tk.tumelo.shared.core.person.CreatePersonPermission;
import tk.tumelo.shared.core.person.DeletePersonPermission;
import tk.tumelo.shared.core.person.IPersonProcessService;
import tk.tumelo.shared.core.person.PersonFormData;
import tk.tumelo.shared.core.person.ReadPersonPermission;
import tk.tumelo.shared.core.person.UpdatePersonPermission;
import tk.tumelo.shared.core.ui.common.fields.AbstractAddressBoxData.Address.AddressRowData;

/**
 * @author sixkn_000
 */
public class PersonProcessService extends AbstractService implements IPersonProcessService {

  @Override
  public PersonFormData create(PersonFormData formData) throws ProcessingException {
    if (!ACCESS.check(new CreatePersonPermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    Person person = new Person();
    person.setFirstName(formData.getFirstName().getValue());
    person.setLastName(formData.getLastName().getValue());
    person.setTitle(formData.getTitle().getValue());
    person.setSalutation(formData.getSalutation().getValue());
    person.setNotes(formData.getNotes().getValue());
    for (AddressRowData rowData : formData.getAddressBox().getAddress().getRows()) {
      Address addr = new Address();
      addr.setId(new ObjectId().toHexString());
      switch (rowData.getRowState()) {
        case AddressRowData.STATUS_INSERTED:
        case AddressRowData.STATUS_UPDATED:
        case AddressRowData.STATUS_NON_CHANGED:
          addr.setPhoneNumberMobile(rowData.getPhoneNumberMobile());
          addr.setPhoneNumberPrivate(rowData.getPhoneNumberPrivate());
          addr.setStreet(rowData.getStreet());
          addr.setZipCode(rowData.getZipCode());
          addr.setAdditionalName(rowData.getAdditionalName());
          addr.setCity(rowData.getCity());
          addr.setEmail(rowData.getEmail());
          addr.setAddressType(rowData.getAddressType());

          person.getAddresses().add(addr);
      }
    }

    MONGO.save(person);

    return formData;
  }

  @Override
  public PersonFormData load(PersonFormData formData) throws ProcessingException {
    if (!ACCESS.check(new ReadPersonPermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    Person person = MONGO.load(Person.class, formData.getPersonNr());
    formData.getFirstName().setValue(person.getFirstName());
    formData.getLastName().setValue(person.getLastName());
    formData.getTitle().setValue(person.getTitle());
    formData.getSalutation().setValue(person.getSalutation());
    formData.getNotes().setValue(person.getNotes());
    for (Address addr : person.getAddresses()) {
      AddressRowData rowData = formData.getAddressBox().getAddress().addRow();
      rowData.setAdditionalName(addr.getAdditionalName());
      rowData.setAddressId(addr.getId().toHexString());
      rowData.setAddressType(addr.getAddressType());
      rowData.setCity(addr.getCity());
      rowData.setEmail(addr.getEmail());
      rowData.setPhoneNumberMobile(addr.getPhoneNumberMobile());
      rowData.setPhoneNumberPrivate(addr.getPhoneNumberPrivate());
      rowData.setStreet(addr.getStreet());
      rowData.setZipCode(addr.getZipCode());
      rowData.setRowState(AddressRowData.STATUS_NON_CHANGED);
    }
    return formData;
  }

  @Override
  public PersonFormData prepareCreate(PersonFormData formData) throws ProcessingException {
    if (!ACCESS.check(new CreatePersonPermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    // TODO [sixkn_000] business logic here.
    return formData;
  }

  @Override
  public PersonFormData store(PersonFormData formData) throws ProcessingException {
    if (!ACCESS.check(new UpdatePersonPermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    Person person = new Person();
    person.setId(formData.getPersonNr());
    person.setFirstName(formData.getFirstName().getValue());
    person.setLastName(formData.getLastName().getValue());
    person.setTitle(formData.getTitle().getValue());
    person.setSalutation(formData.getSalutation().getValue());
    person.setNotes(formData.getNotes().getValue());
    for (AddressRowData rowData : formData.getAddressBox().getAddress().getRows()) {
      Address addr = new Address();
      switch (rowData.getRowState()) {
        case AddressRowData.STATUS_INSERTED:
        case AddressRowData.STATUS_UPDATED:
        case AddressRowData.STATUS_NON_CHANGED:
          addr.setId(StringUtility.isNullOrEmpty(rowData.getAddressId()) ? new ObjectId().toHexString() : rowData.getAddressId());
          addr.setPhoneNumberMobile(rowData.getPhoneNumberMobile());
          addr.setPhoneNumberPrivate(rowData.getPhoneNumberPrivate());
          addr.setStreet(rowData.getStreet());
          addr.setZipCode(rowData.getZipCode());
          addr.setAdditionalName(rowData.getAdditionalName());
          addr.setCity(rowData.getCity());
          addr.setEmail(rowData.getEmail());
          addr.setAddressType(rowData.getAddressType());

          person.getAddresses().add(addr);
      }
    }
    MONGO.store(person);
    return formData;
  }

  @Override
  public void delete(List<String> entityIds) throws ProcessingException {
    if (!ACCESS.check(new DeletePersonPermission())) {
      throw new VetoException(TEXTS.get("AuthorizationFailed"));
    }
    MONGO.delete(Person.class, entityIds);
  }
}
