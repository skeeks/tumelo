/**
 *
 */
package tk.tumelo.client.core.person;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.annotations.PageData;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBox;
import org.eclipse.scout.rt.extension.client.ui.action.menu.AbstractExtensibleMenu;
import org.eclipse.scout.rt.extension.client.ui.basic.table.AbstractExtensibleTable;
import org.eclipse.scout.rt.extension.client.ui.desktop.outline.pages.AbstractExtensiblePageWithTable;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.service.SERVICES;

import tk.tumelo.client.core.person.PersonTablePage.Table;
import tk.tumelo.shared.core.person.IPersonPageService;
import tk.tumelo.shared.core.person.IPersonProcessService;
import tk.tumelo.shared.core.person.PersonTablePageData;

/**
 * @author sixkn_000
 */
@PageData(PersonTablePageData.class)
public class PersonTablePage extends AbstractExtensiblePageWithTable<Table> {

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Persons");
  }

  @Override
  protected void execLoadData(SearchFilter filter) throws ProcessingException {
    PersonTablePageData pageData = SERVICES.getService(IPersonPageService.class).getTablePageData();
    importPageData(pageData);
  }

  @Order(1000.0)
  public class Table extends AbstractExtensibleTable {

    /**
     * @return the LastNameColumn
     */
    public LastNameColumn getLastNameColumn() {
      return getColumnSet().getColumnByClass(LastNameColumn.class);
    }

    /**
     * @return the PersonNrColumn
     */
    public PersonNrColumn getPersonNrColumn() {
      return getColumnSet().getColumnByClass(PersonNrColumn.class);
    }

    @Override
    protected boolean getConfiguredAutoResizeColumns() {
      return true;
    }

    /**
     * @return the FirstNameColumn
     */
    public FirstNameColumn getFirstNameColumn() {
      return getColumnSet().getColumnByClass(FirstNameColumn.class);
    }

    @Order(0.0)
    public class PersonNrColumn extends AbstractStringColumn {

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

      @Override
      protected boolean getConfiguredPrimaryKey() {
        return true;
      }
    }

    @Order(1000.0)
    public class FirstNameColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("FirstName");
      }
    }

    @Order(2000.0)
    public class LastNameColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("LastName");
      }
    }

    @Order(1000.0)
    public class NewPersonMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.EmptySpace);
      }

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("NewPerson");
      }

      @Override
      protected void execAction() throws ProcessingException {
        PersonForm form = new PersonForm();
        form.startNew();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(2000.0)
    public class EditPersonMenu extends AbstractExtensibleMenu {

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("EditPerson");
      }

      @Override
      protected void execAction() throws ProcessingException {
        PersonForm form = new PersonForm();
        form.setPersonNr(getTable().getPersonNrColumn().getSelectedValue());
        form.startModify();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(3000.0)
    public class DeletePersonMenu extends AbstractExtensibleMenu {

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("DeletePerson");
      }

      @Override
      protected void execAction() throws ProcessingException {
        List<String> items = new ArrayList<String>();
        for (ITableRow row : getTable().getSelectedRows()) {
          items.add(getTable().getLastNameColumn().getValue(row) + ", " + getTable().getFirstNameColumn().getValue(row));
        }
        if (MessageBox.showDeleteConfirmationMessage(TEXTS.get("Persons"), items)) {
          SERVICES.getService(IPersonProcessService.class).delete(getTable().getPersonNrColumn().getSelectedValues());
          reloadPage();
        }
      }
    }
  }
}
