/**
 *
 */
package tk.tumelo.client.core.person;

import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.client.ui.form.fields.tabbox.AbstractTabBox;
import org.eclipse.scout.rt.client.ui.form.fields.textfield.AbstractTextField;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.service.SERVICES;

import tk.tumelo.client.core.person.PersonForm.MainBox.CancelButton;
import tk.tumelo.client.core.person.PersonForm.MainBox.DetailBox;
import tk.tumelo.client.core.person.PersonForm.MainBox.DetailBox.FirstNameField;
import tk.tumelo.client.core.person.PersonForm.MainBox.DetailBox.LastNameField;
import tk.tumelo.client.core.person.PersonForm.MainBox.DetailBox.SalutationField;
import tk.tumelo.client.core.person.PersonForm.MainBox.DetailBox.TitleField;
import tk.tumelo.client.core.person.PersonForm.MainBox.OkButton;
import tk.tumelo.client.core.person.PersonForm.MainBox.TabBox;
import tk.tumelo.client.core.person.PersonForm.MainBox.TabBox.AddressBox;
import tk.tumelo.client.core.person.PersonForm.MainBox.TabBox.NotesBox;
import tk.tumelo.client.core.person.PersonForm.MainBox.TabBox.NotesBox.NotesField;
import tk.tumelo.client.core.ui.common.fields.AbstractAddressBox;
import tk.tumelo.client.core.ui.common.fields.AbstractAddressBox.AddressField;
import tk.tumelo.client.core.ui.common.fields.AbstractAddressBox.NewButton;
import tk.tumelo.shared.core.person.IPersonProcessService;
import tk.tumelo.shared.core.person.PersonFormData;
import tk.tumelo.shared.core.person.UpdatePersonPermission;
import tk.tumelo.shared.core.services.code.SalutationCodeType;
import tk.tumelo.shared.core.services.mongodb.annotation.DatabaseField;

/**
 * @author sixkn_000
 */
@FormData(value = PersonFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class PersonForm extends AbstractForm {

  private String m_personNr;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public PersonForm() throws ProcessingException {
    super();
  }

  /**
   * @return the PersonNr
   */
  @FormData
  public String getPersonNr() {
    return m_personNr;
  }

  /**
   * @param personNr
   *          the PersonNr to set
   */
  @FormData
  public void setPersonNr(String personNr) {
    m_personNr = personNr;
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Person");
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startModify() throws ProcessingException {
    startInternal(new ModifyHandler());
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startNew() throws ProcessingException {
    startInternal(new NewHandler());
  }

  /**
   * @return the AddressBox
   */
  public AddressBox getAddressBox() {
    return getFieldByClass(AddressBox.class);
  }

  /**
   * @return the SalutationField
   */
  public SalutationField getSalutationField() {
    return getFieldByClass(SalutationField.class);
  }

  /**
   * @return the TabBox
   */
  public TabBox getTabBox() {
    return getFieldByClass(TabBox.class);
  }

  /**
   * @return the TitleField
   */
  public TitleField getTitleField() {
    return getFieldByClass(TitleField.class);
  }

  /**
   * @return the AddressField
   * @deprecated Use {@link #getAddressBox()#getAddressField()}
   */
  @Deprecated
  public AddressField getAddressField() {
    return getAddressBox().getAddressField();
  }

  /**
   * @return the CancelButton
   */
  public CancelButton getCancelButton() {
    return getFieldByClass(CancelButton.class);
  }

  /**
   * @return the DetailBox
   */
  public DetailBox getDetailBox() {
    return getFieldByClass(DetailBox.class);
  }

  /**
   * @return the FirstNameField
   */
  public FirstNameField getFirstNameField() {
    return getFieldByClass(FirstNameField.class);
  }

  /**
   * @return the LastNameField
   */
  public LastNameField getLastNameField() {
    return getFieldByClass(LastNameField.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the NewButton
   * @deprecated Use {@link #getAddressBox()#getNewButton()}
   */
  @Deprecated
  public NewButton getNewButton() {
    return getAddressBox().getNewButton();
  }

  /**
   * @return the NotesBox
   */
  public NotesBox getNotesBox() {
    return getFieldByClass(NotesBox.class);
  }

  /**
   * @return the NotesField
   */
  public NotesField getNotesField() {
    return getFieldByClass(NotesField.class);
  }

  /**
   * @return the OkButton
   */
  public OkButton getOkButton() {
    return getFieldByClass(OkButton.class);
  }

  @Order(1000.0)
  public class MainBox extends AbstractGroupBox {

    @Order(3000.0)
    public class DetailBox extends AbstractGroupBox {

      @Order(2000.0)
      public class LastNameField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("LastName");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }

      @Order(3000.0)
      @DatabaseField
      public class FirstNameField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("FirstName");
        }
      }

      @Order(4000.0)
      public class SalutationField extends AbstractSmartField<String> {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Salutation");
        }

        @Override
        protected Class<? extends ICodeType<?, String>> getConfiguredCodeType() {
          return SalutationCodeType.class;
        }
      }

      @Order(5000.0)
      public class TitleField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Title");
        }
      }
    }

    @Order(4000.0)
    public class TabBox extends AbstractTabBox {
      @Order(1000.0)
      public class AddressBox extends AbstractAddressBox {

      }

      @Order(2000.0)
      public class NotesBox extends AbstractGroupBox {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Notes");
        }

        @Order(1000.0)
        public class NotesField extends AbstractTextField {

          @Override
          protected double getConfiguredGridWeightX() {
            return 1.0;
          }

          @Override
          protected double getConfiguredGridWeightY() {
            return 1.0;
          }

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Notes");
          }

          @Override
          protected boolean getConfiguredLabelVisible() {
            return false;
          }

          @Override
          protected boolean getConfiguredMultilineText() {
            return true;
          }

          @Override
          protected boolean getConfiguredWrapText() {
            return true;
          }
        }
      }
    }

    @Order(100000.0)
    public class OkButton extends AbstractOkButton {
    }

    @Order(101000.0)
    public class CancelButton extends AbstractCancelButton {
    }
  }

  public class ModifyHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      IPersonProcessService service = SERVICES.getService(IPersonProcessService.class);
      PersonFormData formData = new PersonFormData();
      exportFormData(formData);
      formData = service.load(formData);
      importFormData(formData);
      setEnabledPermission(new UpdatePersonPermission());

    }

    @Override
    protected void execStore() throws ProcessingException {
      IPersonProcessService service = SERVICES.getService(IPersonProcessService.class);
      PersonFormData formData = new PersonFormData();
      exportFormData(formData);
      formData = service.store(formData);

    }
  }

  public class NewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      IPersonProcessService service = SERVICES.getService(IPersonProcessService.class);
      PersonFormData formData = new PersonFormData();
      exportFormData(formData);
      formData = service.prepareCreate(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      IPersonProcessService service = SERVICES.getService(IPersonProcessService.class);
      PersonFormData formData = new PersonFormData();
      exportFormData(formData);
      formData = service.create(formData);

    }
  }
}
