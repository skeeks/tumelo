/**
 *
 */
package tk.tumelo.client.core.codes;

import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.bigdecimalfield.AbstractBigDecimalField;
import org.eclipse.scout.rt.client.ui.form.fields.booleanfield.AbstractBooleanField;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.colorpickerfield.AbstractColorField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.labelfield.AbstractLabelField;
import org.eclipse.scout.rt.client.ui.form.fields.sequencebox.AbstractSequenceBox;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.client.ui.form.fields.tabbox.AbstractTabBox;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.service.SERVICES;

import tk.tumelo.client.core.codes.CodeForm.MainBox.CancelButton;
import tk.tumelo.client.core.codes.CodeForm.MainBox.OkButton;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.AppearanceBox;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.AppearanceBox.BackgroundColorField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.AppearanceBox.ForegroundColorField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.AppearanceBox.IconIdField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.ConfigurationBox;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.ConfigurationBox.ExtensionKeyField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.ConfigurationBox.ValueField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.VisibilityBox;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.VisibilityBox.ActiveBox;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.VisibilityBox.ActiveBox.ActiveField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.VisibilityBox.ActiveBox.ActiveLabelField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.VisibilityBox.EnabledBox;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.VisibilityBox.EnabledBox.EnabledField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TabBox.VisibilityBox.EnabledBox.EnabledLabelField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TranslationsBox;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TranslationsBox.EnglishTextField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TranslationsBox.FrenchTextField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TranslationsBox.GermanTextField;
import tk.tumelo.client.core.codes.CodeForm.MainBox.TranslationsBox.ItalianTextField;
import tk.tumelo.client.core.person.PersonForm.MainBox.DetailBox;
import tk.tumelo.shared.core.codes.CodeFormData;
import tk.tumelo.shared.core.codes.ICodeProcessService;
import tk.tumelo.shared.core.codes.UpdateCodePermission;
import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;

/**
 * @author sixkn_000
 */
@FormData(value = CodeFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class CodeForm extends AbstractForm {

  private String m_codeNr;
  private String m_codeTypeNr;
  private Class<? extends AbstractDatabaseCodeType> m_codeTypeClass;

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public CodeForm() throws ProcessingException {
    super();
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Code");
  }

  /**
   * @return the CodeNr
   */
  @FormData
  public String getCodeNr() {
    return m_codeNr;
  }

  /**
   * @param codeNr
   *          the CodeNr to set
   */
  @FormData
  public void setCodeNr(String codeNr) {
    m_codeNr = codeNr;
  }

  /**
   * @return the CodeTypeClass
   */
  @FormData
  public Class<? extends AbstractDatabaseCodeType> getCodeTypeClass() {
    return m_codeTypeClass;
  }

  /**
   * @param codeTypeClass
   *          the CodeTypeClass to set
   */
  @FormData
  public void setCodeTypeClass(Class<? extends AbstractDatabaseCodeType> codeTypeClass) {
    m_codeTypeClass = codeTypeClass;
  }

  /**
   * @return the CodeTypeNr
   */
  @FormData
  public String getCodeTypeNr() {
    return m_codeTypeNr;
  }

  /**
   * @param codeTypeNr
   *          the CodeTypeNr to set
   */
  @FormData
  public void setCodeTypeNr(String codeTypeNr) {
    m_codeTypeNr = codeTypeNr;
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startModify() throws ProcessingException {
    startInternal(new ModifyHandler());
  }

  /**
   * @throws org.eclipse.scout.commons.exception.ProcessingException
   */
  public void startNew() throws ProcessingException {
    startInternal(new NewHandler());
  }

  /**
   * @return the ActiveBox
   */
  public ActiveBox getActiveBox() {
    return getFieldByClass(ActiveBox.class);
  }

  /**
   * @return the ActiveField
   */
  public ActiveField getActiveField() {
    return getFieldByClass(ActiveField.class);
  }

  /**
   * @return the ActiveLabelField
   */
  public ActiveLabelField getActiveLabelField(){
    return getFieldByClass(ActiveLabelField.class);
  }

  /**
   * @return the AppearanceBox
   */
  public AppearanceBox getAppearanceBox() {
    return getFieldByClass(AppearanceBox.class);
  }

  /**
   * @return the BackgroundColorField
   */
  public BackgroundColorField getBackgroundColorField() {
    return getFieldByClass(BackgroundColorField.class);
  }

  /**
   * @return the CancelButton
   */
  public CancelButton getCancelButton() {
    return getFieldByClass(CancelButton.class);
  }

  /**
   * @return the ConfigurationBox
   */
  public ConfigurationBox getConfigurationBox() {
    return getFieldByClass(ConfigurationBox.class);
  }

  /**
   * @return the DetailBox
   */
  public DetailBox getDetailBox() {
    return getFieldByClass(DetailBox.class);
  }

  /**
   * @return the EnabledBox
   */
  public EnabledBox getEnabledBox() {
    return getFieldByClass(EnabledBox.class);
  }

  /**
   * @return the EnabledField
   */
  public EnabledField getEnabledField() {
    return getFieldByClass(EnabledField.class);
  }

  /**
   * @return the EnabledLabelField
   */
  public EnabledLabelField getEnabledLabelField(){
    return getFieldByClass(EnabledLabelField.class);
  }

  /**
   * @return the EnglishTextField
   */
  public EnglishTextField getEnglishTextField() {
    return getFieldByClass(EnglishTextField.class);
  }

  /**
   * @return the ExtensionKeyField
   */
  public ExtensionKeyField getExtensionKeyField() {
    return getFieldByClass(ExtensionKeyField.class);
  }

  /**
   * @return the ForegroundColorField
   */
  public ForegroundColorField getForegroundColorField() {
    return getFieldByClass(ForegroundColorField.class);
  }

  /**
   * @return the FrenchTextField
   */
  public FrenchTextField getFrenchTextField() {
    return getFieldByClass(FrenchTextField.class);
  }

  /**
   * @return the GermanTextField
   */
  public GermanTextField getGermanTextField() {
    return getFieldByClass(GermanTextField.class);
  }

  /**
   * @return the IconIdField
   */
  public IconIdField getIconIdField() {
    return getFieldByClass(IconIdField.class);
  }

  /**
   * @return the ItalianTextField
   */
  public ItalianTextField getItalianTextField() {
    return getFieldByClass(ItalianTextField.class);
  }

  /**
   * @return the MainBox
   */
  public MainBox getMainBox() {
    return getFieldByClass(MainBox.class);
  }

  /**
   * @return the OkButton
   */
  public OkButton getOkButton() {
    return getFieldByClass(OkButton.class);
  }

  /**
   * @return the TabBox
   */
  public TabBox getTabBox() {
    return getFieldByClass(TabBox.class);
  }

  /**
   * @return the TranslationsBox
   */
  public TranslationsBox getTranslationsBox() {
    return getFieldByClass(TranslationsBox.class);
  }

  /**
   * @return the ValueField
   */
  public ValueField getValueField() {
    return getFieldByClass(ValueField.class);
  }

  /**
   * @return the VisibilityBox
   */
  public VisibilityBox getVisibilityBox() {
    return getFieldByClass(VisibilityBox.class);
  }

  @Order(1000.0)
  public class MainBox extends AbstractGroupBox {

    @Order(1000.0)
    public class TranslationsBox extends AbstractGroupBox {

      @Override
      protected int getConfiguredGridH() {
        return 4;
      }

      @Override
      protected String getConfiguredLabel() {
        return TEXTS.get("Translations");
      }

      @Order(1000.0)
      public class EnglishTextField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("English");
        }

        @Override
        protected boolean getConfiguredMandatory() {
          return true;
        }
      }

      @Order(2000.0)
      public class GermanTextField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("German");
        }
      }

      @Order(3000.0)
      public class FrenchTextField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("French");
        }
      }

      @Order(4000.0)
      public class ItalianTextField extends AbstractStringField {

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Italian");
        }
      }
    }

    @Order(2000.0)
    public class TabBox extends AbstractTabBox {

      @Override
      protected int getConfiguredGridH() {
        return 3;
      }

      @Order(1000.0)
      public class ConfigurationBox extends AbstractGroupBox {

        @Override
        protected int getConfiguredGridH() {
          return 2;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Configuration");
        }

        @Order(1000.0)
        public class ValueField extends AbstractBigDecimalField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("Value");
          }
        }

        @Order(2000.0)
        public class ExtensionKeyField extends AbstractStringField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("ExtensionKey");
          }
        }
      }

      @Order(2000.0)
      public class AppearanceBox extends AbstractGroupBox {

        @Override
        protected int getConfiguredGridH() {
          return 2;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Appearance");
        }

        @Order(2000.0)
        public class IconIdField extends AbstractStringField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("IconId");
          }
        }

        @Order(2000.0)
        public class ForegroundColorField extends AbstractColorField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("ForegroundColor");
          }
        }

        @Order(3000.0)
        public class BackgroundColorField extends AbstractColorField {

          @Override
          protected String getConfiguredLabel() {
            return TEXTS.get("BackgroundColor");
          }
        }
      }

      @Order(3000.0)
      public class VisibilityBox extends AbstractGroupBox {

        @Override
        protected int getConfiguredGridH() {
          return 2;
        }

        @Override
        protected String getConfiguredLabel() {
          return TEXTS.get("Visibility");
        }

        @Order(3000.0)
        public class EnabledBox extends AbstractSequenceBox {

          @Override
          protected int getConfiguredGridW() {
            return 2;
          }

          @Override
          protected boolean getConfiguredLabelVisible() {
            return false;
          }

          @Order(1000.0)
          public class EnabledField extends AbstractBooleanField {

            @Override
            protected String getConfiguredLabel() {
              return TEXTS.get("Enabled");
            }
          }

          @Order(2000.0)
          public class EnabledLabelField extends AbstractLabelField {

            @Override
            protected String getConfiguredLabel() {
              return TEXTS.get("Enabled");
            }
          }
        }

        @Order(4000.0)
        public class ActiveBox extends AbstractSequenceBox {

          @Override
          protected int getConfiguredGridW() {
            return 2;
          }

          @Override
          protected boolean getConfiguredLabelVisible() {
            return false;
          }

          @Order(1000.0)
          public class ActiveField extends AbstractBooleanField {

            @Override
            protected String getConfiguredLabel() {
              return TEXTS.get("Active");
            }
          }

          @Order(2000.0)
          public class ActiveLabelField extends AbstractLabelField {

            @Override
            protected String getConfiguredLabel() {
              return TEXTS.get("Active");
            }
          }
        }
      }
    }

    @Order(100000.0)
    public class OkButton extends AbstractOkButton {
    }

    @Order(101000.0)
    public class CancelButton extends AbstractCancelButton {
    }
  }

  public class ModifyHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ICodeProcessService service = SERVICES.getService(ICodeProcessService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.load(formData);
      importFormData(formData);
      setEnabledPermission(new UpdateCodePermission());

    }

    @Override
    protected void execStore() throws ProcessingException {
      ICodeProcessService service = SERVICES.getService(ICodeProcessService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.store(formData);

    }
  }

  public class NewHandler extends AbstractFormHandler {

    @Override
    protected void execLoad() throws ProcessingException {
      ICodeProcessService service = SERVICES.getService(ICodeProcessService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.prepareCreate(formData);
      importFormData(formData);

    }

    @Override
    protected void execStore() throws ProcessingException {
      ICodeProcessService service = SERVICES.getService(ICodeProcessService.class);
      CodeFormData formData = new CodeFormData();
      exportFormData(formData);
      formData = service.create(formData);

    }
  }
}
