/**
 *
 */
package tk.tumelo.client.core.ui.desktop.outlines;

import org.eclipse.scout.rt.extension.client.ui.desktop.outline.AbstractExtensibleOutline;
import org.eclipse.scout.rt.shared.TEXTS;

import tk.tumelo.shared.core.security.AdministrationOutlinePermission;
import java.util.List;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;

/**
 * @author sixkn_000
 */
public class AdministrationOutline extends AbstractExtensibleOutline {

  /**
   *
   */
  public AdministrationOutline() {
    super();
    setVisiblePermission(new AdministrationOutlinePermission());
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Administration");
  }

  @Override
  protected void execCreateChildPages(List<IPage> pageList) throws ProcessingException {
    CodeTypesTablePage codeTypesTablePage = new CodeTypesTablePage();
    pageList.add(codeTypesTablePage);
  }

}
