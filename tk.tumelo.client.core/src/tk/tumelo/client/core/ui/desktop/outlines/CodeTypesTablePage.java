/**
 *
 */
package tk.tumelo.client.core.ui.desktop.outlines;

import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.annotations.PageData;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.extension.client.ui.basic.table.AbstractExtensibleTable;
import org.eclipse.scout.rt.extension.client.ui.desktop.outline.pages.AbstractExtensiblePageWithTable;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.service.SERVICES;

import tk.tumelo.client.core.ui.desktop.outlines.CodeTypesTablePage.Table;
import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;
import tk.tumelo.shared.core.services.codetypes.ICodePageService;
import tk.tumelo.shared.core.services.codetypes.ReadCodePagePermission;
import tk.tumelo.shared.core.ui.desktop.outlines.CodeTypesTablePageData;

/**
 * @author sixkn_000
 */
@PageData(CodeTypesTablePageData.class)
public class CodeTypesTablePage extends AbstractExtensiblePageWithTable<Table> {

  public CodeTypesTablePage() {
    super();
    setVisiblePermission(new ReadCodePagePermission());
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("CodeTypes");
  }

  @Override
  protected IPage execCreateChildPage(ITableRow row) throws ProcessingException {
    CodesTablePage childPage = new CodesTablePage();
    childPage.setCodeTypeNr(getTable().getCodeTypeNrColumn().getValue(row));
    childPage.setCodeTypeClass(getTable().getCodeTypeClassColumn().getValue(row));
    return childPage;
  }

  @Override
  protected void execLoadData(SearchFilter filter) throws ProcessingException {
    CodeTypesTablePageData pageData = SERVICES.getService(ICodePageService.class).getCodeTypesTableData();
    importPageData(pageData);
  }

  @Order(1000.0)
  public class Table extends AbstractExtensibleTable {

    /**
     * @return the NameColumn
     */
    public NameColumn getNameColumn() {
      return getColumnSet().getColumnByClass(NameColumn.class);
    }

    /**
     * @return the CodeTypeClassColumn
     */
    public CodeTypeClassColumn getCodeTypeClassColumn() {
      return getColumnSet().getColumnByClass(CodeTypeClassColumn.class);
    }

    /**
     * @return the CodeTypeNrColumn
     */
    public CodeTypeNrColumn getCodeTypeNrColumn() {
      return getColumnSet().getColumnByClass(CodeTypeNrColumn.class);
    }

    @Order(1000.0)
    public class CodeTypeNrColumn extends AbstractStringColumn {

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

      @Override
      protected boolean getConfiguredPrimaryKey() {
        return true;
      }
    }

    @Order(2000.0)
    public class NameColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Name");
      }

      @Override
      protected int getConfiguredWidth() {
        return 200;
      }
    }

    @Order(3000.0)
    public class CodeTypeClassColumn extends AbstractColumn<Class<? extends AbstractDatabaseCodeType>> {

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

    }
  }
}
