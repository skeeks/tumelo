package tk.tumelo.client.core.ui.desktop.outlines;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.scout.commons.BooleanUtility;
import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.action.menu.ActivityMapMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TreeMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.ValueFieldMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBigDecimalColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractNumberColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.form.fields.numberfield.INumberField;
import org.eclipse.scout.rt.client.ui.messagebox.MessageBox;
import org.eclipse.scout.rt.extension.client.ui.action.menu.AbstractExtensibleMenu;
import org.eclipse.scout.rt.extension.client.ui.basic.table.AbstractExtensibleTable;
import org.eclipse.scout.rt.extension.client.ui.desktop.outline.pages.AbstractExtensiblePageWithTable;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.service.SERVICES;

import tk.tumelo.client.core.codes.CodeForm;
import tk.tumelo.client.core.ui.desktop.outlines.CodesTablePage.Table;
import tk.tumelo.shared.core.codes.ICodeProcessService;
import tk.tumelo.shared.core.services.code.AbstractDatabaseCodeType;
import tk.tumelo.shared.core.services.codetypes.ICodePageService;
import tk.tumelo.shared.core.services.codetypes.ReadCodePagePermission;
import tk.tumelo.shared.core.ui.desktop.outlines.CodesTablePageData;

/**
 * @author sixkn_000
 */
//@PageData(CodesTablePageData.class) //prevent auto update of pagedata
public class CodesTablePage extends AbstractExtensiblePageWithTable<Table> {

  private String m_codeTypeNr;
  private Class<? extends AbstractDatabaseCodeType> m_codeTypeClass;
  private boolean m_addingNewCodesAllowed;

  public CodesTablePage() {
    super();
    setVisiblePermission(new ReadCodePagePermission());
  }

  @Override
  protected String getConfiguredTitle() {
    return TEXTS.get("Codes");
  }

  @Override
  protected void execLoadData(SearchFilter filter) throws ProcessingException {
    CodesTablePageData pageData = SERVICES.getService(ICodePageService.class).getCodesTableData(getCodeTypeNr(), getCodeTypeClass());
    importPageData(pageData);
    setAddingNewCodesAllowed(pageData.getAddingNewCodesAllowed());
  }

  /**
   * @return the IsAddingNewCodesAllowed
   */
  @FormData
  public boolean isAddingNewCodesAllowed() {
    return m_addingNewCodesAllowed;
  }

  /**
   * @param isAddingNewCodesAllowed
   *          the IsAddingNewCodesAllowed to set
   */
  @FormData
  public void setAddingNewCodesAllowed(boolean isAddingNewCodesAllowed) {
    m_addingNewCodesAllowed = isAddingNewCodesAllowed;
  }

  /**
   * @return the CodeTypeClass
   */
  @FormData
  public Class<? extends AbstractDatabaseCodeType> getCodeTypeClass() {
    return m_codeTypeClass;
  }

  /**
   * @param codeTypeClass
   *          the CodeTypeClass to set
   */
  @FormData
  public void setCodeTypeClass(Class<? extends AbstractDatabaseCodeType> codeTypeClass) {
    m_codeTypeClass = codeTypeClass;
  }

  /**
   * @return the CodeTypeNr
   */
  @FormData
  public String getCodeTypeNr() {
    return m_codeTypeNr;
  }

  /**
   * @param codeTypeNr
   *          the CodeTypeNr to set
   */
  @FormData
  public void setCodeTypeNr(String codeTypeNr) {
    m_codeTypeNr = codeTypeNr;
  }

  @Order(1000.0)
  public class Table extends AbstractExtensibleTable {

    @Override
    protected boolean getConfiguredAutoResizeColumns() {
      return true;
    }

    /**
     * @return the OrderColumn
     */
    public OrderColumn getOrderColumn() {
      return getColumnSet().getColumnByClass(CodesTablePage.Table.OrderColumn.class);
    }

    /**
     * @return the CodeIdColumn
     */
    public CodeIdColumn getCodeIdColumn() {
      return getColumnSet().getColumnByClass(CodeIdColumn.class);
    }

    /**
     * @return the EnglishTextColumn
     */
    public EnglishTextColumn getEnglishTextColumn() {
      return getColumnSet().getColumnByClass(EnglishTextColumn.class);
    }

    /**
     * @return the ExtensionKeyColumn
     */
    public ExtensionKeyColumn getExtensionKeyColumn() {
      return getColumnSet().getColumnByClass(ExtensionKeyColumn.class);
    }

    /**
     * @return the FrenchColumn
     */
    public FrenchColumn getFrenchColumn() {
      return getColumnSet().getColumnByClass(CodesTablePage.Table.FrenchColumn.class);
    }

    /**
     * @return the GermanTextColumn
     */
    public GermanTextColumn getGermanTextColumn() {
      return getColumnSet().getColumnByClass(CodesTablePage.Table.GermanTextColumn.class);
    }

    /**
     * @return the ItalianColumn
     */
    public ItalianColumn getItalianColumn() {
      return getColumnSet().getColumnByClass(ItalianColumn.class);
    }

    /**
     * @return the ProtectedColumn
     */
    public ProtectedColumn getProtectedColumn() {
      return getColumnSet().getColumnByClass(CodesTablePage.Table.ProtectedColumn.class);
    }

    /**
     * @return the ValueColumn
     */
    public ValueColumn getValueColumn() {
      return getColumnSet().getColumnByClass(ValueColumn.class);
    }

    @Order(0.0)
    public class CodeIdColumn extends AbstractStringColumn {

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

      @Override
      protected boolean getConfiguredPrimaryKey() {
        return true;
      }
    }

    @Order(500.0)
    public class OrderColumn extends AbstractBigDecimalColumn {

      @Override
      protected boolean getConfiguredAlwaysIncludeSortAtBegin() {
        return true;
      }

      @Override
      protected boolean getConfiguredDisplayable() {
        return false;
      }

      @Override
      protected int getConfiguredSortIndex() {
        return 0;
      }
    }

    @Order(1000.0)
    public class EnglishTextColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("English");
      }
    }

    @Order(2000.0)
    public class GermanTextColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("German");
      }
    }

    @Order(3000.0)
    public class FrenchColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("French");
      }

      @Override
      protected boolean getConfiguredVisible() {
        return false;
      }
    }

    @Order(4000.0)
    public class ItalianColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Italian");
      }

      @Override
      protected boolean getConfiguredVisible() {
        return false;
      }
    }

    @Order(5000.0)
    public class ExtensionKeyColumn extends AbstractStringColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("ExtensionKey");
      }

      @Override
      protected boolean getConfiguredVisible() {
        return false;
      }
    }

    @Order(6000.0)
    public class ValueColumn extends AbstractNumberColumn<BigDecimal> {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Value");
      }

      @Override
      protected BigDecimal getConfiguredMaxValue() {
        return new BigDecimal(Double.MIN_VALUE);
      }

      @Override
      protected BigDecimal getConfiguredMinValue() {
        return new BigDecimal(Double.MAX_VALUE);
      }

      @Override
      protected INumberField<BigDecimal> getEditorField() {
        return null;

      }
    }

    @Order(7000.0)
    public class ProtectedColumn extends AbstractBooleanColumn {

      @Override
      protected String getConfiguredHeaderText() {
        return TEXTS.get("Protected");
      }

      @Override
      protected boolean getConfiguredVisible() {
        return false;
      }
    }

    @Order(1000.0)
    public class NewCodeMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TableMenuType.EmptySpace);
      }

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("NewCode");
      }

      @Override
      protected void execAboutToShow() throws ProcessingException {
        setVisibleGranted(BooleanUtility.nvl(isAddingNewCodesAllowed()));
      }

      @Override
      protected void execAction() throws ProcessingException {
        CodeForm form = new CodeForm();
        form.setCodeTypeNr(getCodeTypeNr());
        form.setCodeTypeClass(getCodeTypeClass());
        form.startNew();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(2000.0)
    public class EditCodeMenu extends AbstractExtensibleMenu {

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("EditCode");
      }

      @Override
      protected void execAboutToShow() throws ProcessingException {
        setEnabledGranted(!BooleanUtility.nvl(getTable().getProtectedColumn().getSelectedValue()));
      }

      @Override
      protected void execAction() throws ProcessingException {
        CodeForm form = new CodeForm();
        form.setCodeNr(getTable().getCodeIdColumn().getSelectedValue());
        form.setCodeTypeNr(getCodeTypeNr());
        form.setCodeTypeClass(getCodeTypeClass());
        form.startModify();
        form.waitFor();
        if (form.isFormStored()) {
          reloadPage();
        }
      }
    }

    @Order(3000.0)
    public class DeleteMenu extends AbstractExtensibleMenu {

      @Override
      protected Set<? extends IMenuType> getConfiguredMenuTypes() {
        return CollectionUtility.<IMenuType> hashSet(TreeMenuType.MultiSelection, TreeMenuType.SingleSelection, ValueFieldMenuType.NotNull, TableMenuType.MultiSelection, TableMenuType.SingleSelection, ActivityMapMenuType.Activity);
      }

      @Override
      protected String getConfiguredText() {
        return TEXTS.get("DeleteMenu");
      }

      @Override
      protected void execAboutToShow() throws ProcessingException {
        setEnabledGranted(!BooleanUtility.nvl(getTable().getProtectedColumn().getSelectedValue()));
      }

      @Override
      protected void execAction() throws ProcessingException {
        // Display error if rows are selected which are protected. (not deletable)
        for (ITableRow row : getSelectedRows()) {
          if (BooleanUtility.nvl(getTable().getProtectedColumn().getValue(row))) {
            MessageBox.showOkMessage(TEXTS.get("ApplicationTitle"), TEXTS.get("UnerasableDatarecordsSelected"), null);
            return;
          }
        }

        List<String> items = new ArrayList<String>();
        for (ITableRow row : getSelectedRows()) {
          items.add(getTable().getEnglishTextColumn().getValue(row) + "(" + getTable().getGermanTextColumn().getValue(row) + ")");
        }

        if (MessageBox.showDeleteConfirmationMessage(TEXTS.get("Codes"), items)) {
          SERVICES.getService(ICodeProcessService.class).delete(getTable().getCodeIdColumn().getSelectedValues(), getCodeTypeClass());
          reloadPage();
        }
      }
    }
  }
}
