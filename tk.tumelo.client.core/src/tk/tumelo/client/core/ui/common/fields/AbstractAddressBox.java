/**
 *
 */
package tk.tumelo.client.core.ui.common.fields;

import java.util.Set;

import org.eclipse.scout.commons.CollectionUtility;
import org.eclipse.scout.commons.annotations.FormData;
import org.eclipse.scout.commons.annotations.Order;
import org.eclipse.scout.commons.exception.ProcessingException;
import org.eclipse.scout.rt.client.ui.action.menu.ActivityMapMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.CalendarMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TreeMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.ValueFieldMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractSmartColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractLinkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.tablefield.AbstractTableField;
import org.eclipse.scout.rt.extension.client.ui.action.menu.AbstractExtensibleMenu;
import org.eclipse.scout.rt.extension.client.ui.basic.table.AbstractExtensibleTable;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;

import tk.tumelo.shared.core.services.code.PersonAddressTypeCodeType;
import tk.tumelo.shared.core.ui.common.fields.AbstractAddressBoxData;

/**
 * @author sixkn_000
 */
@FormData(value = AbstractAddressBoxData.class, sdkCommand = FormData.SdkCommand.CREATE, defaultSubtypeSdkCommand = FormData.DefaultSubtypeSdkCommand.CREATE)
public abstract class AbstractAddressBox extends AbstractGroupBox {
  @Override
  protected boolean getConfiguredFillVertical() {
    return false;
  }

  @Override
  protected String getConfiguredLabel() {
    return TEXTS.get("Address");
  }

  /**
   * @return the AddressField
   */
  public AddressField getAddressField() {
    return getFieldByClass(AddressField.class);
  }

  /**
   * @return the NewButton
   */
  public NewButton getNewButton() {
    return getFieldByClass(NewButton.class);
  }

  @Order(1000.0)
  public class AddressField extends AbstractTableField<AddressField.Table> {

    public void addNewAddressRow() throws ProcessingException {
      ITableRow row = getTable().createRow();
      getTable().addRow(row);
    }

    public void removeAddressRow(ITableRow row) {
      getTable().deleteRow(row);
    }

    @Override
    protected int getConfiguredGridH() {
      return 5;
    }

    @Override
    protected String getConfiguredLabel() {
      return TEXTS.get("Address");
    }

    @Override
    protected boolean getConfiguredLabelVisible() {
      return false;
    }

    @Order(1000.0)
    public class Table extends AbstractExtensibleTable {

      /**
       * @return the StreetColumn
       */
      public StreetColumn getStreetColumn() {
        return getColumnSet().getColumnByClass(StreetColumn.class);
      }

      /**
       * @return the ZipCodeColumn
       */
      public ZipCodeColumn getZipCodeColumn() {
        return getColumnSet().getColumnByClass(ZipCodeColumn.class);
      }

      /**
       * @return the CityColumn
       */
      public CityColumn getCityColumn() {
        return getColumnSet().getColumnByClass(CityColumn.class);
      }

      /**
       * @return the PhoneNumberPrivateColumn
       */
      public PhoneNumberPrivateColumn getPhoneNumberPrivateColumn() {
        return getColumnSet().getColumnByClass(PhoneNumberPrivateColumn.class);
      }

      /**
       * @return the PhoneNumberMobileColumn
       */
      public PhoneNumberMobileColumn getPhoneNumberMobileColumn() {
        return getColumnSet().getColumnByClass(PhoneNumberMobileColumn.class);
      }

      /**
       * @return the EmailColumn
       */
      public EmailColumn getEmailColumn() {
        return getColumnSet().getColumnByClass(EmailColumn.class);
      }

      /**
       * @return the AddressTypeColumn
       */
      public AddressTypeColumn getAddressTypeColumn() {
        return getColumnSet().getColumnByClass(AddressTypeColumn.class);
      }

      @Override
      protected boolean getConfiguredAutoResizeColumns() {
        return true;
      }

      /**
       * @return the AdditionalNameColumn
       */
      public AdditionalNameColumn getAdditionalNameColumn() {
        return getColumnSet().getColumnByClass(AdditionalNameColumn.class);
      }

      /**
       * @return the AddressIdColumn
       */
      public AddressIdColumn getAddressIdColumn() {
        return getColumnSet().getColumnByClass(AddressIdColumn.class);
      }

      @Order(1000.0)
      public class AddressIdColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredDisplayable() {
          return false;
        }

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected boolean getConfiguredPrimaryKey() {
          return true;
        }
      }

      @Order(1500.0)
      public class AddressTypeColumn extends AbstractSmartColumn<String> {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("AddressType");
        }

        @Override
        protected Class<? extends ICodeType<?, String>> getConfiguredCodeType() {
          return PersonAddressTypeCodeType.class;
        }
      }

      @Order(2000.0)
      public class AdditionalNameColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("AdditionalName");
        }
      }

      @Order(3000.0)
      public class StreetColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("Street");
        }
      }

      @Order(4000.0)
      public class ZipCodeColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("ZipCodePost");
        }
      }

      @Order(5000.0)
      public class CityColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("City");
        }
      }

      @Order(7000.0)
      public class PhoneNumberPrivateColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("PhoneNumberPrivate");
        }
      }

      @Order(8000.0)
      public class PhoneNumberMobileColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("PhoneNumberMobile");
        }
      }

      @Order(9000.0)
      public class EmailColumn extends AbstractStringColumn {

        @Override
        protected boolean getConfiguredEditable() {
          return true;
        }

        @Override
        protected String getConfiguredHeaderText() {
          return TEXTS.get("Email");
        }
      }

      @Order(1000.0)
      public class NewMenu extends AbstractExtensibleMenu {

        @Override
        protected Set<? extends IMenuType> getConfiguredMenuTypes() {
          return CollectionUtility.<IMenuType> hashSet(TreeMenuType.EmptySpace, ValueFieldMenuType.Null, TableMenuType.EmptySpace, ActivityMapMenuType.Selection, CalendarMenuType.EmptySpace);
        }

        @Override
        protected String getConfiguredText() {
          return TEXTS.get("NewMenu");
        }

        @Override
        protected void execAction() throws ProcessingException {
          addNewAddressRow();
        }
      }

      @Order(2000.0)
      public class DeleteMenu extends AbstractExtensibleMenu {

        @Override
        protected String getConfiguredText() {
          return TEXTS.get("DeleteMenu");
        }

        @Override
        protected void execAction() throws ProcessingException {
          removeAddressRow(getSelectedRow());
        }

      }
    }
  }

  @Order(2000.0)
  public class NewButton extends AbstractLinkButton {

    @Override
    protected String getConfiguredLabel() {
      return TEXTS.get("NewMenu");
    }

    @Override
    protected void execClickAction() throws ProcessingException {
      getAddressField().addNewAddressRow();
    }
  }

}
