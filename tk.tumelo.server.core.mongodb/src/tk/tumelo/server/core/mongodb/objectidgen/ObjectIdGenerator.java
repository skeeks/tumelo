package tk.tumelo.server.core.mongodb.objectidgen;

import org.bson.types.ObjectId;

/**
 * This script can be used to generator ids for code types and codes.
 * @author sixkn_000
 *
 */
public class ObjectIdGenerator {

	public static void main(String[] args) {
		System.out.println("Generating unique id for mongo database");
		System.out.println(new ObjectId().toHexString());
	}

}
