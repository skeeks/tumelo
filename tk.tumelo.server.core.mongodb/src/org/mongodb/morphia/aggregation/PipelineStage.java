package org.mongodb.morphia.aggregation;

public class PipelineStage {
    @SuppressWarnings("unused")
	private final String name;

    public PipelineStage(final String name) {
        this.name = name;
    }
}
