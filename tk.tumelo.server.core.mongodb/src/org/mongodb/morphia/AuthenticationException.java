package org.mongodb.morphia;


@SuppressWarnings("serial")
public class AuthenticationException extends RuntimeException {

  public AuthenticationException(final String msg) {
    super(msg);
  }
}
